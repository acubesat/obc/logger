#include <Logger.hpp>

etl::format_spec Logger::format;

Logger::LogEntry::LogEntry(LogLevel level) : level(level) {}

Logger::LogEntry::~LogEntry() {
	// When the destructor is called, the log message is fully "designed". 
        // The destructor enables the message to be "displayed" to the user.
	Logger::log(level, message);
}

template <>
void convertValueToString(String<LOGGER_MAX_MESSAGE_SIZE>& message,
                          float value) {
   etl::to_string(value, message, Logger::format.precision(Logger::MaxPrecision),
	               true);
}

template <>
void convertValueToString(String<LOGGER_MAX_MESSAGE_SIZE>& message,
						  double value) {
    etl::to_string(value, message, Logger::format.precision(Logger::MaxPrecision),
				   true);
}

template<>
void convertValueToString(String<LOGGER_MAX_MESSAGE_SIZE>& message, LoggingHelpers::CANParameterUpdate value) {
	message.append("Parameter [");
	etl::to_string(value.ParameterID, message, true);
	message.append("] update from CAN bus: ");
	etl::to_string(value.oldValue, message, Logger::format.precision(Logger::MaxPrecision), true);
	message.append(" -> ");
	etl::to_string(value.newValue, message, Logger::format.precision(Logger::MaxPrecision), true);
}

template <SizeOfParametersArray SIZE>
void convertValueToString(
    String<LOGGER_MAX_MESSAGE_SIZE>& message, LoggingHelpers::CANParametersRequest<SIZE> value) {
	message.append("Requesting parameters with IDs: ");

	bool isFirst = true;

	for (const auto& id: value.ParameterIDs) {
		if (id == 0)
			continue;
		if (!isFirst) {
			message.append(", ");
		}

		isFirst = false;

		etl::to_string(id, message, true);
	}
}

template void convertValueToString(String<LOGGER_MAX_MESSAGE_SIZE>&, LoggingHelpers::CANParametersRequest<LOGGER_MAX_PARAMETER_REQUEST_SIZE>);

template<Logger::LogLevel errorLevel>
void etlErrorCallback(const etl::exception& etlError){
	Logger::LogEntry(errorLevel) << "ETL Error: " <<etlError.what() << "\n"
								 << " in file: "    << etlError.file_name() << "\n"
								 << " at line: "     << etlError.line_number();
}