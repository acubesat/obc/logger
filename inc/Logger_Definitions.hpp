#pragma once
#include <cstdint>

/**
 * @defgroup LoggerDefinitions ECSS Defined Constants
 *
 * This file contains constant definitions that are used in the logger configuration.
 *
 * @todo All these constants need to be redefined and revised after the design and the requirements are finalized.
 *
 * @{
 */

/**
 * @file
 * This file contains constant definitions that are used in the logger configuration.
 * @see LoggerDefinitions
 */

/**
 * @brief Maximum size of a log message
 */
#define LOGGER_MAX_MESSAGE_SIZE 512

/**
 * @brief Maximum requested parameters array
 */
#define LOGGER_MAX_PARAMETER_REQUEST_SIZE 10

/**
* @brief Size of request parameters array
*/
typedef  uint16_t SizeOfRequestParametersArray;

/**
* @brief Size of parameters array
*/
typedef uint16_t SizeOfParametersArray;

/**
* @brief Size of parameters
*/
typedef uint16_t SizeOfParameters;