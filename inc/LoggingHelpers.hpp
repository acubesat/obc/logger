#pragma once

#include <etl/array.h>

namespace LoggingHelpers {

	typedef struct CANParameterUpdate {
        SizeOfParameters ParameterID;
        double oldValue;
        double newValue;

        CANParameterUpdate(SizeOfParameters paramID, double oldVal, double newVal)
            : ParameterID(paramID), oldValue(oldVal), newValue(newVal) {
        }
    } CANParameterUpdate;

    template <SizeOfParametersArray SIZE>
    struct CANParametersRequest {
        const etl::array<SizeOfRequestParametersArray, SIZE>& ParameterIDs;

        explicit CANParametersRequest(const etl::array<SizeOfRequestParametersArray, SIZE>& ids)
            : ParameterIDs(ids) {
        }
    };
}
